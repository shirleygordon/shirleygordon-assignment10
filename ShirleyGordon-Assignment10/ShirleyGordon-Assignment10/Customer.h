#pragma once
#include"Item.h"
#include<set>

using std::string;

class Customer
{
public:
	Customer(string name);
	Customer() {};
	double totalSum() const;//returns the total sum for payment
	void addItem(Item itemToAdd);//add item to the set
	void removeItem(Item itemToRemove);//remove item from the set

	//get and set functions
	string& getName();
	std::set<Item>& getItems();
	void setName(string name);

private:
	std::string _name;
	std::set<Item> _items;
};
