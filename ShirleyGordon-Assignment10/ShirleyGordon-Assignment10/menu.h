#pragma once
#include <iostream>
#include <iomanip>
#include <map>
#include "Customer.h"
#include "inputException.h"

using std::cout;
using std::endl;
using std::cin;

#define NUM_PADDING 4

enum mainMenuOptions{BUY_ITEMS = 1, UPDATE_CART, PRINT_CUSTOMER, EXIT};
enum updateCartOptions { ADD_ITEMS = 1, REMOVE_ITEMS, BACK_TO_MENU };

class menu
{
private:
	static bool isChoiceValid(int& choice, int min, int max);
	static void printMenu();
	static void printUpdateCartMenu();
	static void printCart(const std::set<Item>& cart);
	static bool getItemToAddChoice(const Item itemList[], const int size, Item& itemToAdd);
	static bool getItemToRemoveChoice(const std::set<Item>& cart, Item& itemToRemove);

public:
	static void mainMenu(int& choice);
	static void isNameValid(const string& name, std::map<string, Customer>& abcCustomers);
	static void printItems(const Item itemsList[], const int size);
	static void updateCartMenu(int& choice);
	static void addItems(const Item itemList[], const int size, const string& customerName, std::map<string, Customer>& abcCustomers);
	static void removeItems(const Item itemList[], const int size, const string& customerName, std::map<string, Customer>& abcCustomers);
	static void printCustomerWhoPaysTheMost(std::map<string, Customer>& abcCustomers);
};