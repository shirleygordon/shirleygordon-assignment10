#include "menu.h"

#define NUM_OF_ITEMS 10

int main()
{
	int choice = 0;
	string customerName = "";
	std::map<string, Customer> abcCustomers;
	Item itemList[NUM_OF_ITEMS] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("Bread","00003",8.9),
		Item("Chocolate","00004",7.0),
		Item("Cheese","00005",15.3),
		Item("Rice","00006",6.2),
		Item("Fish", "00008", 31.65),
		Item("Chicken","00007",25.99),
		Item("Cucumber","00009",1.21),
		Item("Tomato","00010",2.32)};

	do 
	{
		menu::mainMenu(choice); // Print the program's main menu and and get user's choice

		switch (choice)
		{
			case BUY_ITEMS:

				// Get new customer's name
				cout << "Enter name: ";
				cin >> customerName;

				try
				{
					// If name is valid, insert customer into customers map.
					menu::isNameValid(customerName, abcCustomers);
					abcCustomers[customerName] = Customer(customerName);
				}
				catch (string & e)
				{
					cout << e << endl;
					system("PAUSE");
					break;
				}

				// Print all available items and add items to user's cart according to input.
				menu::printItems(itemList, NUM_OF_ITEMS);
				menu::addItems(itemList, NUM_OF_ITEMS, customerName, abcCustomers);

				break;

			case UPDATE_CART:

				if (!abcCustomers.empty()) // If there's at least one customer:
				{
					// Get customer's name
					cout << "Enter name: ";
					cin >> customerName;

					try // If name is valid, it means it doesn't exist.
					{
						menu::isNameValid(customerName, abcCustomers);
						cout << "Name doesn't exist. Please try again." << endl;
						system("PAUSE");
						break;
					}
					catch (string & e) {} // If exception was thrown, name was found.

					while (choice != BACK_TO_MENU)
					{
						menu::updateCartMenu(choice); // Print the menu and get user's choice.

						switch (choice)
						{
						case ADD_ITEMS:

							// Print all available items and add items to user's cart according to input.
							menu::printItems(itemList, NUM_OF_ITEMS);
							menu::addItems(itemList, NUM_OF_ITEMS, customerName, abcCustomers);

							break;

						case REMOVE_ITEMS:

							// Print all available items and add items to user's cart according to input.
							menu::removeItems(itemList, NUM_OF_ITEMS, customerName, abcCustomers);

							break;
						}
					}
				}
				else // If there are no customers:
				{
					cout << "There are no customers." << endl;
					system("PAUSE");
				}

				break;

			case PRINT_CUSTOMER:

				menu::printCustomerWhoPaysTheMost(abcCustomers);

				break;
		}
	}
	while (choice != EXIT);

	return 0;
}