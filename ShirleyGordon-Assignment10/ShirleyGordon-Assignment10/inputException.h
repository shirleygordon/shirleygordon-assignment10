#pragma once
#include <exception>
#include <iostream>

class inputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "Invalid input! Please enter again:";
	}

	static void checkFailState();
	static void clearIstream();
	static void getNumericInput(double& input);
	static void getNumericInput(int& input);
};