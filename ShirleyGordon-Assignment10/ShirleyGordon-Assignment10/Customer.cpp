#include "Customer.h"

/*
C'tor for customer object.
Input: name string.
Output: none.
*/
Customer::Customer(std::string name)
{
	_name = name;
}

/*
Function returns the customer's total sum for payment.
Input: none.
Output: the customer's total sum for payment.
*/
double Customer::totalSum() const
{
	double totalSum = 0;
	std::set<Item>::iterator itemsIt;

	// Iterate through all the customer's items and add up their prices to the total sum.
	for (itemsIt = _items.begin(); itemsIt != _items.end(); itemsIt++)
	{
		totalSum += (*itemsIt).totalPrice();
	}

	return totalSum;
}

/*
Function adds a new item to the customer's cart.
Input: new item to add.
Output: none.
*/
void Customer::addItem(Item itemToAdd)
{
	// Check if the item to add is already in the cart.
	std::set<Item>::iterator itemIt = std::find(_items.begin(), _items.end(), itemToAdd);

	if (itemIt == _items.end()) // Item wasn't found in the cart.
	{
		// Add the item to the cart
		_items.insert(itemToAdd);
	}
	else // Item is already in the cart.
	{
		itemToAdd.setCount((*itemIt).getCount() + 1); // Set the item's count to the old count + 1.
		_items.erase(itemToAdd); // Remove the old item from cart.
		_items.insert(itemToAdd); // Insert the same item with a higher count.
	}
}

/*
Function removes an item from the customer's cart.
Input: item to remove.
Output: none.
*/
void Customer::removeItem(Item itemToRemove)
{
	// Check if the item to remove exists in the cart.
	std::set<Item>::iterator itemIt = std::find(_items.begin(), _items.end(), itemToRemove);

	if (itemIt != _items.end()) // If the item exists in the cart.
	{
		if ((*itemIt).getCount() == 1) // If there's only 1 item of this type,
		{
			_items.erase(itemToRemove); // Remove the item from the cart completely.
		}
		else
		{
			itemToRemove.setCount((*itemIt).getCount() - 1); // Set the item's count to the old count - 1.
			_items.erase(itemToRemove); // Remove the old item from cart.
			_items.insert(itemToRemove); // Insert the same item with a lower count.
		}
	}
}

std::set<Item>& Customer::getItems()
{
	return this->_items;
}
