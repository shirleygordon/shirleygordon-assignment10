#include "menu.h"

/*
Function checks if the user's choice is valid.
Input: user's choice.
Output: true if the choice is valid, false otherwise.
*/
bool menu::isChoiceValid(int& choice, int min, int max)
{
	return (choice >= min && choice <= max);
}

/*
Function prints the program's menu.
Input: none.
Output: none.
*/
void menu::printMenu()
{
	cout << "Welcome to MagshiMart!" << endl
		<< "1. to sign as customer and buy items" << endl
		<< "2. to update existing customer's items" << endl
		<< "3. to print the customer who pays the most" << endl
		<< "4. to exit" << endl;
}

/*
Function prints the menu for the update cart option.
Input: none.
Output: none.
*/
void menu::printUpdateCartMenu()
{
	cout << "1. Add items" << endl
		 << "2. Remove items" << endl
		 << "3. Back to menu" << endl;
}

/*
Function prints the items in the customer's cart.
Input: none.
Output: none.
*/
void menu::printCart(const std::set<Item>& cart)
{
	std::set<Item>::iterator cartIt;
	unsigned int i = 1;

	// Print all the items in the customer's cart, the amount of each item and the total price.
	for (cartIt = cart.begin(); cartIt != cart.end(); cartIt++)
	{
		cout << std::setw(NUM_PADDING) << i << ". " << (*cartIt).getName() << "		" << "amount: " << (*cartIt).getCount() << "	" << "total price: " << (*cartIt).totalPrice() << endl;
		i++;
	}
}

/*
Function gets user's choice for item to add.
Input: items array, size of items array, reference to item to add.
Output: none.
*/
bool menu::getItemToAddChoice(const Item itemList[], const int size, Item& itemToAdd)
{
	int itemChoice = 0;
	bool getAgain = false, exit = false;

	do
	{
		getAgain = false;

		// Get user's input for item choice
		inputException::getNumericInput(itemChoice);

		// If the number the user entered isn't valid, get input again.
		if (itemChoice < 0 || itemChoice > size)
		{
			cout << "Please enter number from 0-" << size << "." << endl;
			getAgain = true;
		}
	} while (getAgain);

	if (itemChoice) // Not 0 (exit)
	{
		// Create the item to add
		itemToAdd.setName(itemList[itemChoice - 1].getName());
		itemToAdd.setSerialNumber(itemList[itemChoice - 1].getSerialNumber());
		itemToAdd.setUnitPrice(itemList[itemChoice - 1].getUnitPrice());
	}
	else // If the user chose to exit.
	{
		exit = true;
	}

	return exit;
}

/*
Function gets user's choice for item to remove.
Input: items set (customer's cart), reference to item to remove.
Output: none.
*/
bool menu::getItemToRemoveChoice(const std::set<Item>& cart, Item& itemToRemove)
{
	int itemChoice = 0;
	unsigned int i = 0;
	bool getAgain = false, exit = false;
	std::set<Item>::iterator cartIt = cart.begin();

	do
	{
		getAgain = false;

		// Get user's input for item choice
		inputException::getNumericInput(itemChoice);

		// If the number the user entered isn't valid, get input again.
		if (itemChoice < 0 || itemChoice > cart.size())
		{
			cout << "Please enter number from 0-" << cart.size() << "." << endl;
			getAgain = true;
		}
	} while (getAgain);

	if (itemChoice) // Not 0 (exit)
	{
		for (i = 0; i < itemChoice - 1; i++)
		{
			cartIt++; // Make the cart iterator point to the item to remove.
		}
		
		// Create the item to remove.
		itemToRemove.setName((*cartIt).getName());
		itemToRemove.setSerialNumber((*cartIt).getSerialNumber());
		itemToRemove.setUnitPrice((*cartIt).getUnitPrice());
	}
	else // If the user chose to exit.
	{
		exit = true;
	}

	return exit;
}

/*
Function prints the main menu of the program and gets the user's choice.
Input: choice reference.
Output: none.
*/
void menu::mainMenu(int& choice)
{
	bool validChoice = true;

	do
	{
		// Clear the screen and print menu
		system("cls");
		printMenu();

		// Get user's choice
		inputException::getNumericInput(choice);
		validChoice = isChoiceValid(choice, BUY_ITEMS, EXIT);
	}
	while (!validChoice);
}

/*
Function checks if the new customer's name is valid.
Input: customer name reference, customers map reference.
Output: none.
*/
void menu::isNameValid(const string& name, std::map<string, Customer>& abcCustomers)
{
	std::map<string, Customer>::iterator customersIt;

	// Iterate through all customers and check if name already exists.
	for (customersIt = abcCustomers.begin(); customersIt != abcCustomers.end(); customersIt++)
	{
		if (name == (*customersIt).first) // If name already exists, throw exception.
		{
			throw string("Error! Name already taken.");
		}
	}
}

/*
Function prints all the available items.
Input: items array, items array size.
Output: none.
*/
void menu::printItems(const Item itemsList[], const int size)
{
	unsigned int i = 0;

	cout << "The items you can buy are: (0 to exit)" << endl;

	// Print all items and prices
	for (i = 0; i < size; i++)
	{
		cout << std::setw(NUM_PADDING) << (i + 1) << ". " << itemsList[i].getName() << "		" << "price: " << itemsList[i].getUnitPrice() << endl;
	}

	cout << "Which item/s would you like to buy? Input:" << endl;
}

/*
Function prints the menu for the update cart option, and gets user's choice.
Input: choice reference.
Output: none.
*/
void menu::updateCartMenu(int& choice)
{
	bool validChoice = true;

	do
	{
		// Clear the screen and print menu
		system("cls");
		printUpdateCartMenu();

		// Get user's choice
		inputException::getNumericInput(choice);
		validChoice = isChoiceValid(choice, ADD_ITEMS, BACK_TO_MENU);
	}
	while (!validChoice);
}

/*
Function gets user's input and adds items to their cart.
Input: items array, size of items array, customer's name reference, customers map reference.
Output: none.
*/
void menu::addItems(const Item itemList[], const int size, const string& customerName, std::map<string, Customer>& abcCustomers)
{
	Item itemToAdd("", "", 1);
	bool exit = false;

	// Get user's item choice.
	do
	{
		exit = menu::getItemToAddChoice(itemList, size, itemToAdd);

		// If the user didn't choose to exit, add the item to cart.
		if (!exit)
		{
			abcCustomers[customerName].addItem(itemToAdd);
		}
	} while (!exit);
}

/*
Function gets user's input and removes items from their cart.
Input: items array, size of items array, customer's name reference, customers map reference.
Output: none.
*/
void menu::removeItems(const Item itemList[], const int size, const string& customerName, std::map<string, Customer>& abcCustomers)
{
	Item itemToRemove("", "", 1);
	bool exit = false;

	// Get user's item choice.
	do
	{
		// Print the customer's cart
		system("cls");
		cout << "Items in your cart:" << endl;
		printCart(abcCustomers[customerName].getItems());
		cout << "Which item/s would you like to remove? Input:" << endl;

		exit = menu::getItemToRemoveChoice(abcCustomers[customerName].getItems(), itemToRemove); // Get user's choice.

		// If the user didn't choose to exit, remove the item from cart.
		if (!exit)
		{
			abcCustomers[customerName].removeItem(itemToRemove);
		}
	} while (!exit);
}

/*
Function prints the details of the customer who pays the most.
Input: const reference of customers map.
Output: none.
*/
void menu::printCustomerWhoPaysTheMost(std::map<string, Customer>& abcCustomers)
{
	std::map<string, Customer>::iterator customersIt;
	std::map<string, Customer>::iterator maxCustomerIt = abcCustomers.begin(); // Iterator to the customer who pays the most.

	if (!abcCustomers.empty()) // If there is at least one customer
	{
		for (customersIt = abcCustomers.begin(); customersIt != abcCustomers.end(); customersIt++)
		{
			if ((*customersIt).second.totalSum() > (*maxCustomerIt).second.totalSum())
			{
				maxCustomerIt = customersIt;
			}
		}

		cout << "Name: " << (*maxCustomerIt).first << endl << "Items in " << (*maxCustomerIt).first << "'s cart:" << endl << endl;
		printCart((*maxCustomerIt).second.getItems());
		cout << endl << "Total sum: " << (*maxCustomerIt).second.totalSum() << endl;
	}
	else // If there are no customers
	{
		cout << "There are no customers." << endl;
	}

	system("PAUSE");
}
