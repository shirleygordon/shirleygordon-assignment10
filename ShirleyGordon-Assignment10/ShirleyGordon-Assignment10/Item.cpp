#include "Item.h"

/*
C'tor for item object.
Input: name string, serialNumber string, unitPrice.
Output: none.
*/
Item::Item(string name, string serialNumber, double unitPrice)
{
	bool tryAgain = true;

	_name = name;
	_serialNumber = serialNumber;
	_count = 1;

	// Try to set unitPrice. Exception will be thrown if it's lower or equal to 0.
	while (tryAgain)
	{
		try
		{
			this->setUnitPrice(unitPrice);
			tryAgain = false;
		}
		catch (string & e)
		{
			cout << e << endl << "Please enter unit price again: ";
			cin >> unitPrice;
		}
	}
}

/*
Function returns the total price of the item.
Input: none.
Output: the total price of the item.
*/
double Item::totalPrice() const
{
	return _count * _unitPrice;
}

/*
Function returns whether this item is smaller than the other item.
Input: reference to other item.
Output: true if this item is smaller than the other one, false otherwise.
*/
bool Item::operator<(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}

/*
Function returns whether this item is bigger than the other item.
Input: reference to other item.
Output: true if this item is bigger than the other one, false otherwise.
*/
bool Item::operator>(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}

/*
Function returns whether this item is the same as the other item.
Input: reference to other item.
Output: true if this item is the same as the other one, false otherwise.
*/
bool Item::operator==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;;
}

// GETTERS
string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

// SETTERS
void Item::setName(string name)
{
	this->_name = name;
}

void Item::setSerialNumber(string serialNumber)
{
	this->_serialNumber = serialNumber;
}

void Item::setCount(int count)
{
	this->_count = count;
}

void Item::setUnitPrice(double unitPrice)
{
	if (unitPrice > 0)
	{
		this->_unitPrice = unitPrice;
	}
	else // If unitPrice isn't bigger than 0, throw exception.
	{
		throw string("Error! Price has to be larger than 0.");
	}
}
