#include "inputException.h"

/*
Function checks if the input stream is in fail state and throws an exception if it is.
Input: none.
Output: none.
*/
void inputException::checkFailState()
{
	if (std::cin.fail())
	{
		throw inputException();
	}
}

/*
Function gets rid of failure state of input stream.
Input: none.
Output: none.
*/
void inputException::clearIstream()
{
	std::cin.clear(); // Get rid of failure state
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Discard 'bad' character(s) 
}

/*
Function gets double input.
Input: reference to double variable.
Output: none.
*/
void inputException::getNumericInput(double& input)
{
	bool getInputAgain = false;

	do // Get input
	{
		getInputAgain = false;
		try
		{
			std::cin >> input;
			checkFailState(); // Check if the input is valid. If not, throw exception.
		}
		catch (std::exception& e)
		{
			getInputAgain = true;
			std::cout << e.what() << std::endl; // Print exception
			clearIstream(); // Clear input stream and try again
		}
	} while (getInputAgain);
}

/*
Function gets integer input.
Input: reference to integer variable.
Output: none.
*/
void inputException::getNumericInput(int& input)
{
	double doubleInput = 0;

	getNumericInput(doubleInput); // Put input into a double variable.
	input = (int)doubleInput; // Cast to int.
}